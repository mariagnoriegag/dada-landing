import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  count: null,
  type: null,
  typename: '',
  checkbox: [],
  costoconsumo: 0,
  costoreserva: 0,
  costoadicionales: 0,
  costototal: 0,
  salones: [],
  eventos: [],
  carta: {
    bebidas: [],
    comida: []
  },
  backdada: '',
  logodada: ''
}

const mutations = {
  increment (state) {
    state.count++
    mutations.updateConsumoCost(state)
  },
  decrement (state) {
    if (state.count > 0) {
      state.count--
      mutations.updateConsumoCost(state)
    }
  },
  updateCount (state, count) {
    state.count = count
    mutations.updateConsumoCost(state)
  },
  updateType (state, type) {
    state.type = type
    state.typename = type.name
    mutations.updateAdicionalCost(state)
  },
  updateConsumoCost (state) {
    if (state.count !== null) {
      state.costoconsumo = state.count * 50
      mutations.updateTotalCost(state)
    } else {
      state.costoconsumo = 0
      mutations.updateTotalCost(state)
    }
  },
  updateReservaCost (state, a1) {
    if (a1 !== null) {
      state.costoreserva = a1.costo
      mutations.updateTotalCost(state)
    } else {
      state.costoreserva = 0
      mutations.updateTotalCost(state)
    }
  },
  updateAdicionalCost (state) {
    if (state.typename === 'Corporativa') {
      state.costoadicionales = 0
      for (let i = 0; i < state.checkbox.length; i++) {
        if (state.checkbox[i] === 'Fotos') {
          state.costoadicionales += 100
        } if (state.checkbox[i] === 'Diseño') {
          state.costoadicionales += 100
        } if (state.checkbox[i] === 'Presencia de marca') {
          state.costoadicionales += 100
        }
      }
    } else {
      state.costoadicionales = 0
      state.checkbox = []
    }
  },
  updateTotalCost (state) {
    state.costototal = state.costoconsumo + state.costoreserva + state.costoadicionales
  },
  updateCheckbox (state, checkbox) {
    state.checkbox = checkbox
    mutations.updateAdicionalCost(state)
    mutations.updateTotalCost(state)
  },
  updateBackdada (state, backdada) {
    state.backdada = backdada
  },
  updateLogodada (state, logodada) {
    state.logodada = logodada
  },
  updateSalones (state, salones) {
    if (state.salones.length === 0) {
      for (let i = 0; i < salones.length; i++) {
        state.salones.push({...salones[i]})
      }
    } else {
      state.salones = state.salones
    }
  },
  updateEventos (state, eventos) {
    if (state.eventos.length === 0) {
      for (let i = 0; i < eventos.length; i++) {
        state.eventos.push({...eventos[i]})
      }
    } else {
      state.eventos = state.eventos
    }
  },
  updateCarta (state, carta) {
    if (state.carta.bebidas.length === 0) {
      for (let i = 0; i < carta.bebidas.length; i++) {
        state.carta.bebidas.push({...carta.bebidas[i]})
      }
    } else {
      state.carta.bebidas = state.carta.bebidas
    }
    if (state.carta.comida.length === 0) {
      for (let i = 0; i < carta.comida.length; i++) {
        state.carta.comida.push({...carta.comida[i]})
      }
    } else {
      state.carta.comida = state.carta.comida
    }
  }
}

const actions = {
  increment: ({ commit }) => commit('increment'),
  decrement: ({ commit }) => commit('decrement'),
  updateMessage: ({ commit }) => commit('updateMessage'),
  updateType: ({ commit }) => commit('updateType'),
  updateConsumoCost: ({ commit }) => commit('updateConsumoCost'),
  updateReservaCost: ({ commit }) => commit('updateReservaCost'),
  updateAdicionalCost: ({ commit }) => commit('updateAdicionalCost'),
  updateTotalCost: ({ commit }) => commit('updateTotalCost'),
  updateSalones: ({ commit }) => commit('updateSalones'),
  updateEventos: ({ commit }) => commit('updateEventos'),
  updateCarta: ({ commit }) => commit('updateCarta')
}

export default new Vuex.Store({
  state,
  actions,
  mutations
})
