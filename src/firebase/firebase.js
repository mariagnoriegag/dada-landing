import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/storage'

const app = firebase.initializeApp({
  apiKey: 'AIzaSyCyYLhjTAhHe-ZGm0R0AygFwk8K0qJoqxw',
  authDomain: 'dadalanding.firebaseapp.com',
  databaseURL: 'https://dadalanding.firebaseio.com',
  projectId: 'dadalanding',
  storageBucket: 'dadalanding.appspot.com',
  messagingSenderId: '176183092700'
})

export const db = app.database()
export const storage = app.storage()
export const salonesRef = db.ref('salones')
export const eventosRef = db.ref('eventos')
export const cartaRef = db.ref('carta')
