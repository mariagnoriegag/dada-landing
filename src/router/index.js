import Vue from 'vue'
import Router from 'vue-router'
import DadaInicio from '@/components/DadaInicio'
import DadaCartaComida from '@/components/DadaCartaComida'
import DadaCartaBebida from '@/components/DadaCartaBebida'
import DadaCarta from '@/components/DadaCarta'
import DadaReservas from '@/components/DadaReservas'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'DadaInicio',
      component: DadaInicio
    },
    {
      path: '/carta',
      component: DadaCarta,
      children: [
        {
          path: '/',
          component: DadaCartaComida
        },
        {
          path: 'bebida',
          component: DadaCartaBebida
        }
      ]
    },
    {
      path: '/reservas',
      name: 'DadaReservas',
      component: DadaReservas
    }
  ]
})
